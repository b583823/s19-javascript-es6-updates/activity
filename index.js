let num = 2;
let getcube = (number=>number**3);
console.log(`The cube of ${num} is ${getcube(num)}`);

const address = ["258 Wasington Ave", "NW", "California", 90011];
const [street, city, state, zip] = address;

console.log(`I live at ${street} ${city}, ${state} ${zip}`);

class Animal{
	constructor(name,terrain,type,weight,lengthFeet,lengthInch){
		this.name = name;
		this.terrain = terrain;
		this.name = name;
		this.weight = weight;
		this.lengthFeet = lengthFeet;
		this.lengthInch = lengthInch;

	}
}

const croc = new Animal("Lolong","saltwater", "crocodile", 1075, 20,3);
const {name,terrain,type,weight,lengthFeet,lengthInch} = croc;
console.log(`${name} was a ${terrain} ${type}. He weighed at ${weight} kgs with a measurement of ${lengthFeet} ft ${lengthInch} in.`);
let numberArray = [1,2,3,4,5];
numberArray.forEach(number => {
	console.log(number);
})
let totalArray = numberArray.reduce((acc,cur)=>{
	return acc+cur;
})
console.log(totalArray);
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}
const dog = new Dog("Frankie", 5, "Miniature Dachsund");
console.log(dog);